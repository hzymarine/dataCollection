package com.ctjsoft.datacollection.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ctjsoft.datacollection.entity.KDatabaseType;
import com.ctjsoft.datacollection.mapper.KDatabaseTypeMapper;
import com.ctjsoft.datacollection.service.KDatabaseTypeService;
import org.springframework.stereotype.Service;

@Service
public class KDatabaseTypeServiceImpl extends ServiceImpl<KDatabaseTypeMapper, KDatabaseType> implements KDatabaseTypeService {
}
