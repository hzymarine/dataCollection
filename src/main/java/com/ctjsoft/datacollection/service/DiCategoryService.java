package com.ctjsoft.datacollection.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ctjsoft.datacollection.core.povo.Result;
import com.ctjsoft.datacollection.entity.DiCategory;
import com.ctjsoft.datacollection.entity.DiRespository;
import com.ctjsoft.datacollection.entity.KQuartz;
import org.apache.ibatis.annotations.Param;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.repository.AbstractRepository;
import org.pentaho.di.repository.RepositoryDirectoryInterface;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public interface DiCategoryService extends IService<DiCategory> {
    void delete(String id);
    void update(DiCategory diCategory);
    List<DiCategory> findDiCateryByRep(String repId);
    DiRespository findRepositoryById(String id);
    Result add(DiCategory dca);


}
