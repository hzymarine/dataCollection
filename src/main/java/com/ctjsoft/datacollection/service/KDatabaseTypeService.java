package com.ctjsoft.datacollection.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ctjsoft.datacollection.entity.KDatabaseType;

public interface KDatabaseTypeService extends IService<KDatabaseType> {
}
