package com.ctjsoft.datacollection.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ctjsoft.datacollection.core.povo.TreeDTO;
import com.ctjsoft.datacollection.entity.KRepository;
import com.github.pagehelper.PageInfo;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.repository.RepositoryObjectType;

import java.util.List;

public interface KRepositoryService extends IService<KRepository> {

    void add(KRepository repository);

    void delete(String id);

    void deleteBatch(List<String> ids);

    void update(KRepository repository);

    PageInfo<KRepository> findRepListByPage(KRepository repository, Integer page, Integer rows);

    KRepository getRepositoryDetail(String id);

    List<KRepository> findRepList();

    List<TreeDTO<String>> findRepTreeById(String id, RepositoryObjectType objectType);


    List<TreeDTO<String>> findTransRepTreegridById(String id, String dirPath);

    void testConnection(KRepository repository);

    List<TreeDTO<String>> initTransRep(String id, String dirPath);
    /*public KRepository getByRepName(String repName);*/

    JSONArray getDatabasesByRepId(String id) throws KettleException;

    String createRepository(JSONObject jsonObject) throws Exception;
}
