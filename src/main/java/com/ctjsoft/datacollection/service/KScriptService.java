package com.ctjsoft.datacollection.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ctjsoft.datacollection.entity.KScript;
import com.github.pagehelper.PageInfo;
import org.pentaho.di.core.exception.KettleException;

import java.util.List;

public interface KScriptService extends IService<KScript> {

    PageInfo<KScript> findScriptListByPage(KScript script, Integer page, Integer rows);

    String checkCatch(String repId, String tjId, String type, JSONObject para, Integer isStart) throws KettleException;

    void startAllScript();

    void startScript(String id, boolean executeOnce);

    void startCollectionTask(KScript kScript);

    void stopAllScript();

    void stopScript(String id);

    KScript getById(String id);

    List<KScript> taskCount();
}
