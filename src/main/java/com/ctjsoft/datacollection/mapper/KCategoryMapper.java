package com.ctjsoft.datacollection.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ctjsoft.datacollection.entity.KCategory;
import org.apache.ibatis.annotations.Mapper;

public interface KCategoryMapper extends BaseMapper<KCategory> {

}
