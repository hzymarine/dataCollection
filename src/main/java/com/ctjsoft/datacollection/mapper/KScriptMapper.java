package com.ctjsoft.datacollection.mapper;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ctjsoft.datacollection.entity.KScript;

import java.util.List;
public interface KScriptMapper extends BaseMapper<KScript> {

      List<KScript> selectAll(KScript kScript);

      //KScript selectBySid(String id);

      List<KScript> taskCount();

}
