package com.ctjsoft.datacollection.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ctjsoft.datacollection.entity.DiCategory;
import com.ctjsoft.datacollection.entity.DiScript;
import com.ctjsoft.datacollection.entity.KCategory;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;
@Mapper
public interface DiScriptMapper extends BaseMapper<DiScript> {
    void deleteByCategoryIdInAndRepId(@Param("categorys") List<String> categorys,@Param("repId") String repId);
    List<DiScript> findByCategoryIdAndRepId(@Param("categoryId")String categoryId,@Param("repId") String repId);
    List<DiScript> findByCategoryPidAndRepId(@Param("categoryId") String categoryId, @Param("repId") String repId);
    List<DiScript> findByCategoryIdInAndRepId(@Param("categorys")List<String> s,@Param("repId") String repId);
    List<DiScript> findByRepIdAndType(String repId,String type);
    List<DiScript> findByCategoryIdInAndRepIdAndType(List<String> s, String repId, String type);
    List<DiScript> findByRepId(String repId);
    void deleteByRepId(@Param("repId") String repId);
    void insertAll(List<DiScript> diScripts);
}
