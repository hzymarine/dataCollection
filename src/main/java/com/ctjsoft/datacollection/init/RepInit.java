package com.ctjsoft.datacollection.init;

import com.ctjsoft.datacollection.core.povo.TreeDTO;
import com.ctjsoft.datacollection.core.repository.RepositoryUtil;
import com.ctjsoft.datacollection.entity.KRepository;
import com.ctjsoft.datacollection.mapper.KRepositoryMapper;
import com.ctjsoft.datacollection.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.pentaho.di.repository.AbstractRepository;
import org.pentaho.di.repository.RepositoryObjectType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@Order()
@Slf4j
public class RepInit implements CommandLineRunner {

    public static Map<String, List<Integer>> repMap = new HashMap<String, List<Integer>>();

    @Autowired
    private KRepositoryMapper repRepository;


    @Override
    public void run(String... args) throws Exception {
        log.info("系统启动中,缓存repMap");
        init();
    }

    @Scheduled(cron = "0 0/3 * * * ?")
    public void ref() throws Exception {
        log.info("定时更新repMap缓存");
        init();
    }

    private List<TreeDTO<String>> dataFormat(List<TreeDTO<String>> repositoryTreeList) {
        List<TreeDTO<String>> resultList = new ArrayList<>();
        repositoryTreeList.forEach(treeDTO -> {
            String id = treeDTO.getId();
            List<TreeDTO<String>> children = treeDTO.getChildren();
            if (children != null) {
                children.forEach(childrenTree -> {
                    String id1 = childrenTree.getId();
                    if (childrenTree.getChildren() != null) {
                        //末级脚本
                        childrenTree.getChildren().forEach(childrens -> {
                            childrens.setPid(id1);
                            childrens.setChildren(null);
                            resultList.add(childrens);
                        });
                    }
                    childrenTree.setChildren(null);
                    resultList.add(childrenTree);
                    childrenTree.setPid(id);
                });
            }
            treeDTO.setChildren(null);
            resultList.add(treeDTO);
        });
        return resultList;
    }

    private void init() {
        try {
            List<KRepository> repositorieList = repRepository.selectList(null);
            if (repositorieList.size() > 0) {
                repositorieList.forEach(repository -> {
                    AbstractRepository abstractRepository = RepositoryUtil.connection(repository);
                    List<TreeDTO<String>> trans = RepositoryUtil.getRepositoryTreeList(abstractRepository, "/", RepositoryObjectType.TRANSFORMATION);
                    List<Integer> transid = dataFormat(trans).stream().filter(s -> !StringUtil.isEmpty(s.getObjectType())).map(s -> Integer.valueOf(s.getId().substring(s.getId().lastIndexOf("@") + 1, s.getId().length()))).collect(Collectors.toList());
                    repMap.put("rept/" + repository.getId(), transid);
                    List<TreeDTO<String>> jobs = RepositoryUtil.getRepositoryTreeList(abstractRepository, "/", RepositoryObjectType.JOB);
                    List<Integer> jobsid = dataFormat(jobs).stream().filter(s -> !StringUtil.isEmpty(s.getObjectType())).map(s -> Integer.valueOf(s.getId().substring(s.getId().lastIndexOf("@") + 1, s.getId().length()))).collect(Collectors.toList());
                    repMap.put("repj/" + repository.getId(), jobsid);
                });
                log.info("更新完成");
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

}
