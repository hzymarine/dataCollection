package com.ctjsoft.datacollection.init;

import com.ctjsoft.datacollection.service.KScriptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 项目启动完成后重新设置定时任务
 *
 * @author lyf
 */
@Component
@Order(value = 99)
public class QuartzInit implements ApplicationRunner {
    @Autowired
    KScriptService scriptService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        /*scriptService
        jobService.initJobsQuartz();
        transService.initTransQuartz();*/
    }
}
