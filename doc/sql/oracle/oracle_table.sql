create table K_REPOSITORY
(
    ID            VARCHAR2(32) default sys_guid() not null,
    REP_NAME      NVARCHAR2(50),
    REP_TYPE      NVARCHAR2(10),
    REP_USERNAME  NVARCHAR2(50),
    REP_PASSWORD  NVARCHAR2(50),
    REP_BASE_PATH NVARCHAR2(500),
    DB_TYPE       NVARCHAR2(10),
    DB_ACCESS     NVARCHAR2(10),
    DB_HOST       NVARCHAR2(50),
    DB_PORT       NVARCHAR2(10),
    DB_NAME       NVARCHAR2(20),
    DB_USERNAME   NVARCHAR2(50),
    DB_PASSWORD   NVARCHAR2(50),
    ADD_TIME      DATE,
    ADD_USER      VARCHAR2(32),
    EDIT_TIME     DATE,
    EDIT_USER     VARCHAR2(32),
    DEL_FLAG      NUMBER(11)
)
    /

comment on table K_REPOSITORY is '资源库表'
/

comment on column K_REPOSITORY.ID is 'ID'
/

comment on column K_REPOSITORY.REP_NAME is '资源库名称'
/

comment on column K_REPOSITORY.REP_TYPE is '资源库类型: fileRep-文件, dbRep-数据库'
/

comment on column K_REPOSITORY.REP_USERNAME is '登录用户名'
/

comment on column K_REPOSITORY.REP_PASSWORD is '登录密码'
/

comment on column K_REPOSITORY.REP_BASE_PATH is '文件资源库路径: rep_type=fileRep生效'
/

comment on column K_REPOSITORY.DB_TYPE is '资源库数据库类型（MYSQL、ORACLE）'
/

comment on column K_REPOSITORY.DB_ACCESS is '资源库数据库访问模式（Native, ODBC, OCI, Plugin, JNDI)'
/

comment on column K_REPOSITORY.DB_HOST is '资源库数据库主机名或者IP地址'
/

comment on column K_REPOSITORY.DB_PORT is '资源库数据库端口号'
/

comment on column K_REPOSITORY.DB_NAME is '资源库数据库名称'
/

comment on column K_REPOSITORY.DB_USERNAME is '数据库登录账号'
/

comment on column K_REPOSITORY.DB_PASSWORD is '数据库登录密码'
/

comment on column K_REPOSITORY.ADD_TIME is '添加时间'
/

comment on column K_REPOSITORY.ADD_USER is '添加者'
/

comment on column K_REPOSITORY.EDIT_TIME is '编辑时间'
/

comment on column K_REPOSITORY.EDIT_USER is '编辑者'
/

comment on column K_REPOSITORY.DEL_FLAG is '是否删除（0：存在；1：删除）'
/

create unique index REP_NAME_UNIQUE_INDEX
    on K_REPOSITORY (REP_NAME)
/

create table K_CATEGORY
(
    ID             VARCHAR2(32) not null,
    CATEGORY_NAME  NVARCHAR2(50),
    CATEGORY_LEVEL NUMBER(1),
    PARENT_ID      VARCHAR2(32),
    ADD_TIME       DATE,
    ADD_USER       VARCHAR2(32),
    EDIT_TIME      DATE,
    EDIT_USER      VARCHAR2(32),
    DEL_FLAG       NUMBER(11)
)
    /

comment on table K_CATEGORY is '分类表'
/

comment on column K_CATEGORY.ID is '分类ID'
/

comment on column K_CATEGORY.CATEGORY_NAME is '分类名称'
/

comment on column K_CATEGORY.CATEGORY_LEVEL is '分类级次'
/

comment on column K_CATEGORY.PARENT_ID is '父级ID'
/

comment on column K_CATEGORY.ADD_TIME is '添加时间'
/

comment on column K_CATEGORY.ADD_USER is '添加者'
/

comment on column K_CATEGORY.EDIT_TIME is '编辑时间'
/

comment on column K_CATEGORY.EDIT_USER is '编辑者'
/

comment on column K_CATEGORY.DEL_FLAG is '是否删除（0：存在；1：删除）'
/

create unique index CATEGORY_NAME_UNIQUE_INDEX
    on K_CATEGORY (CATEGORY_NAME)
/

create table K_SCRIPT
(
    ID                   VARCHAR2(32),
    CATEGORY_ID          VARCHAR2(32),
    SCRIPT_NAME          VARCHAR2(50),
    SCRIPT_DESCRIPTION   VARCHAR2(500),
    SCRIPT_TYPE          VARCHAR2(10),
    SCRIPT_PATH          VARCHAR2(200),
    SCRIPT_REPOSITORY_ID VARCHAR2(50),
    SCRIPT_QUARTZ        VARCHAR2(50),
    SYNC_STRATEGY        VARCHAR2(50),
    SCRIPT_LOG_LEVEL     VARCHAR2(50),
    SCRIPT_STATUS        VARCHAR2(50),
    ADD_TIME             DATE,
    ADD_USER             VARCHAR2(32),
    EDIT_TIME            DATE,
    EDIT_USER            VARCHAR2(32),
    DEL_FLAG             NUMBER(11),
    SCRIPT_PARAMS        VARCHAR2(2000),
    EXECUTE_TYPE         VARCHAR2(10)
)
    /

comment on table K_SCRIPT is '采集脚本表'
/

comment on column K_SCRIPT.ID is '脚本ID'
/

comment on column K_SCRIPT.CATEGORY_ID is '分类ID（外键ID）'
/

comment on column K_SCRIPT.SCRIPT_NAME is '脚本名称'
/

comment on column K_SCRIPT.SCRIPT_DESCRIPTION is '任务描述'
/

comment on column K_SCRIPT.SCRIPT_TYPE is 'job,trans'
/

comment on column K_SCRIPT.SCRIPT_PATH is '脚本保存路径（可以是资源库中的路径也可以是服务器中保存作业文件的路径）'
/

comment on column K_SCRIPT.SCRIPT_REPOSITORY_ID is '作业的资源库ID'
/

comment on column K_SCRIPT.SCRIPT_QUARTZ is '定时策略（外键ID）'
/

comment on column K_SCRIPT.SYNC_STRATEGY is '同步策略(T+n)'
/

comment on column K_SCRIPT.SCRIPT_LOG_LEVEL is '日志级别(Basic，Detailed，Error，Debug，Minimal，Rowlevel）'
/

comment on column K_SCRIPT.SCRIPT_STATUS is '状态（1：正在运行；2：已停止）'
/

comment on column K_SCRIPT.ADD_TIME is '添加时间'
/

comment on column K_SCRIPT.ADD_USER is '添加者'
/

comment on column K_SCRIPT.EDIT_TIME is '编辑时间'
/

comment on column K_SCRIPT.EDIT_USER is '编辑者'
/

comment on column K_SCRIPT.DEL_FLAG is '是否删除（0：存在；1：删除）'
/

comment on column K_SCRIPT.SCRIPT_PARAMS is '参数'
/

comment on column K_SCRIPT.EXECUTE_TYPE is '执行类型（rep：资源库；file：文件；ftp：FTP）'
/

create table K_DATABASE_TYPE
(
    ID          VARCHAR2(32) not null,
    CODE        NVARCHAR2(255),
    DESCRIPTION NVARCHAR2(255),
    ADD_TIME    DATE,
    ADD_USER    VARCHAR2(32),
    EDIT_TIME   DATE,
    EDIT_USER   VARCHAR2(32),
    DEL_FLAG    NUMBER(11)
)
    /

comment on table K_DATABASE_TYPE is '数据库连接类型表'
/

comment on column K_DATABASE_TYPE.ID is '主键'
/

comment on column K_DATABASE_TYPE.CODE is '编码'
/

comment on column K_DATABASE_TYPE.DESCRIPTION is '描述'
/

comment on column K_DATABASE_TYPE.ADD_TIME is '添加时间'
/

comment on column K_DATABASE_TYPE.ADD_USER is '添加者'
/

comment on column K_DATABASE_TYPE.EDIT_TIME is '编辑时间'
/

comment on column K_DATABASE_TYPE.EDIT_USER is '编辑者'
/

comment on column K_DATABASE_TYPE.DEL_FLAG is '是否删除（0：存在；1：删除）'
/

-- ----------------------------
-- Records of k_database_type
-- ----------------------------
INSERT INTO `k_database_type` VALUES ('1', 'DERBY', 'Apache Derby', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('2', 'AS/400', 'AS/400', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('3', 'INTERBASE', 'Borland Interbase', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('4', 'INFINIDB', 'Calpont InfiniDB', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('5', 'IMPALASIMBA', 'Cloudera Impala', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('6', 'DBASE', 'dBase III, IV or 5', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('7', 'EXASOL4', 'Exasol 4', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('8', 'EXTENDB', 'ExtenDB', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('9', 'FIREBIRD', 'Firebird SQL', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('10', 'GENERIC', 'Generic database', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('11', 'GOOGLEBIGQUERY', 'Google BigQuery', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('12', 'GREENPLUM', 'Greenplum', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('13', 'SQLBASE', 'Gupta SQL Base', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('14', 'H2', 'H2', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('15', 'HIVE', 'Hadoop Hive', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('16', 'HIVE2', 'Hadoop Hive 2/3', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('17', 'HYPERSONIC', 'Hypersonic', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('18', 'DB2', 'IBM DB2', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('19', 'IMPALA', 'Impala', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('20', 'INFOBRIGHT', 'Infobright', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('21', 'INFORMIX', 'Informix', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('22', 'INGRES', 'Ingres', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('23', 'VECTORWISE', 'Ingres VectorWise', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('24', 'CACHE', 'Intersystems Cache', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('25', 'KINGBASEES', 'KingbaseES', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('26', 'LucidDB', 'LucidDB', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('27', 'MARIADB', 'MariaDB', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('28', 'SAPDB', 'MaxDB (SAP DB)', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('29', 'MONETDB', 'MonetDB', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('30', 'MSACCESS', 'MS Access', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('31', 'MSSQL', 'MS SQL Server', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('32', 'MSSQLNATIVE', 'MS SQL Server (Native)', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('33', 'MYSQL', 'MySQL', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('34', 'MONDRIAN', 'Native Mondrian', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('35', 'NEOVIEW', 'Neoview', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('36', 'NETEZZA', 'Netezza', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('37', 'ORACLE', 'Oracle', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('38', 'ORACLERDB', 'Oracle RDB', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('39', 'PALO', 'Palo MOLAP Server', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('40', 'KettleThin', 'Pentaho Data Services', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('41', 'POSTGRESQL', 'PostgreSQL', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('42', 'REDSHIFT', 'Redshift', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('43', 'REMEDY-AR-SYSTEM', 'Remedy Action Request System', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('44', 'SAPR3', 'SAP ERP System', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('45', 'SNOWFLAKEHV', 'Snowflake', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('46', 'SPARKSIMBA', 'SparkSQL', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('47', 'SQLITE', 'SQLite', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('48', 'SYBASE', 'Sybase', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('49', 'SYBASEIQ', 'SybaseIQ', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('50', 'TERADATA', 'Teradata', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('51', 'UNIVERSE', 'UniVerse database', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('52', 'VERTICA', 'Vertica', NULL, NULL, NULL, NULL, 0);
INSERT INTO `k_database_type` VALUES ('53', 'VERTICA5', 'Vertica 5+', NULL, NULL, NULL, NULL, 0);

create table K_SCRIPT_MONITOR
(
    ID                VARCHAR2(32) not null
        primary key,
    MONITOR_SCRIPT_ID VARCHAR2(32),
    MONITOR_SUCCESS   VARCHAR2(32),
    MONITOR_FAIL      VARCHAR2(32),
    MONITOR_STATUS    NUMBER(11),
    RUN_STATUS        NCLOB,
    LAST_EXECUTE_TIME DATE,
    NEXT_EXECUTE_TIME DATE,
    ADD_TIME          DATE,
    ADD_USER          VARCHAR2(32),
    EDIT_TIME         DATE,
    EDIT_USER         VARCHAR2(32),
    DEL_FLAG          NUMBER(11)
)
    /

comment on table K_SCRIPT_MONITOR is '监控表'
/

comment on column K_SCRIPT_MONITOR.ID is '监控ID'
/

comment on column K_SCRIPT_MONITOR.MONITOR_SCRIPT_ID is '监控的脚本的ID'
/

comment on column K_SCRIPT_MONITOR.MONITOR_SUCCESS is '成功次数'
/

comment on column K_SCRIPT_MONITOR.MONITOR_FAIL is '失败次数'
/

comment on column K_SCRIPT_MONITOR.MONITOR_STATUS is '监控状态（是否启动，1:启动；2:停止）'
/

comment on column K_SCRIPT_MONITOR.RUN_STATUS is '运行状态（起始时间-结束时间,起始时间-结束时间……）'
/

comment on column K_SCRIPT_MONITOR.LAST_EXECUTE_TIME is '上一次执行时间'
/

comment on column K_SCRIPT_MONITOR.NEXT_EXECUTE_TIME is '下一次执行时间'
/

comment on column K_SCRIPT_MONITOR.ADD_TIME is '添加时间'
/

comment on column K_SCRIPT_MONITOR.ADD_USER is '添加者'
/

comment on column K_SCRIPT_MONITOR.EDIT_TIME is '编辑时间'
/

comment on column K_SCRIPT_MONITOR.EDIT_USER is '编辑者'
/

comment on column K_SCRIPT_MONITOR.DEL_FLAG is '是否删除（0：存在；1：删除）'
/

create table K_SCRIPT_RECORD
(
    ID               VARCHAR2(32) not null,
    RECORD_SCRIPT_ID VARCHAR2(32),
    START_TIME       DATE,
    STOP_TIME        DATE,
    RECORD_STATUS    NUMBER(11),
    LOG_FILE_PATH    NVARCHAR2(100),
    ADD_TIME         DATE,
    ADD_USER         VARCHAR2(32),
    EDIT_TIME        DATE,
    EDIT_USER        VARCHAR2(32),
    DEL_FLAG         NUMBER(11),
    CATEGORY_ID      VARCHAR2(32)
)
    /

comment on table K_SCRIPT_RECORD is '执行记录表'
/

comment on column K_SCRIPT_RECORD.ID is '记录ID'
/

comment on column K_SCRIPT_RECORD.RECORD_SCRIPT_ID is '脚本ID'
/

comment on column K_SCRIPT_RECORD.START_TIME is '启动时间'
/

comment on column K_SCRIPT_RECORD.STOP_TIME is '停止时间'
/

comment on column K_SCRIPT_RECORD.RECORD_STATUS is '任务执行结果（1：成功；0：失败）'
/

comment on column K_SCRIPT_RECORD.LOG_FILE_PATH is '转换日志记录文件保存位置'
/

comment on column K_SCRIPT_RECORD.ADD_TIME is '添加时间'
/

comment on column K_SCRIPT_RECORD.ADD_USER is '添加者'
/

comment on column K_SCRIPT_RECORD.EDIT_TIME is '编辑时间'
/

comment on column K_SCRIPT_RECORD.EDIT_USER is '编辑者'
/

comment on column K_SCRIPT_RECORD.DEL_FLAG is '是否删除（0：存在；1：删除）'
/

comment on column K_SCRIPT_RECORD.CATEGORY_ID is '分类ID'
/

create table K_LOG
(
    ID          VARCHAR2(32),
    ADMDIVCODE  VARCHAR2(255),
    TYPE        VARCHAR2(255),
    TRANSNAME   VARCHAR2(255),
    STEPNAME    VARCHAR2(255),
    I           NUMBER(32),
    O           NUMBER(32),
    R           NUMBER(32),
    W           NUMBER(32),
    U           NUMBER(32),
    E           NUMBER(32),
    TIME        DATE,
    CATEGORY_ID NUMBER(32)
)
    /

comment on column K_LOG.ID is 'id'
/

comment on column K_LOG.ADMDIVCODE is '区划code'
/

comment on column K_LOG.TYPE is '1为读2为写3为其他'
/

comment on column K_LOG.TRANSNAME is '转换名'
/

comment on column K_LOG.STEPNAME is '步骤名'
/

comment on column K_LOG.I is '当前步骤生成的记录数（从表输出、文件读入）'
/

comment on column K_LOG.O is '当前步骤输出的记录数（输出的文件和表） '
/

comment on column K_LOG.R is '当前步骤从前一步骤读取的记录数 '
/

comment on column K_LOG.W is '当前步骤向后面步骤抛出的记录数 '
/

comment on column K_LOG.U is '当前步骤更新过的记录数 '
/

comment on column K_LOG.E is '当前步骤处理的记录数'
/

comment on column K_LOG.TIME is '完成时间'
/

comment on column K_LOG.CATEGORY_ID is '分类id'
/

create table K_QUARTZ
(
    ID                 VARCHAR2(32) not null,
    QUARTZ_DESCRIPTION NVARCHAR2(500),
    QUARTZ_CRON        NVARCHAR2(100),
    ADD_TIME           DATE,
    ADD_USER           VARCHAR2(32),
    EDIT_TIME          DATE,
    EDIT_USER          VARCHAR2(32),
    DEL_FLAG           NUMBER(11)
)
    /

comment on table K_QUARTZ is '定时任务表'
/

comment on column K_QUARTZ.ID is '任务ID'
/

comment on column K_QUARTZ.QUARTZ_DESCRIPTION is '任务描述'
/

comment on column K_QUARTZ.QUARTZ_CRON is '定时策略'
/

comment on column K_QUARTZ.ADD_TIME is '添加时间'
/

comment on column K_QUARTZ.ADD_USER is '添加者'
/

comment on column K_QUARTZ.EDIT_TIME is '编辑时间'
/

comment on column K_QUARTZ.EDIT_USER is '编辑者'
/

comment on column K_QUARTZ.DEL_FLAG is '是否删除（0：存在；1：删除）'
/

create table DI_CATEGORY
(
    ID           VARCHAR2(32) not null
        constraint SYS_C0092965
        primary key
        constraint SYS_C0092964
        check ("ID" IS NOT NULL),
    NAME         VARCHAR2(255),
    CATEGORY_PID VARCHAR2(32),
    IS_DEFAULT   NUMBER(1),
    REP_ID       VARCHAR2(32),
    PATH         VARCHAR2(255),
    CATEGORY_ID  VARCHAR2(255),
    CODE         VARCHAR2(255)
)
    /

comment on column DI_CATEGORY.ID is '主键id'
/

comment on column DI_CATEGORY.NAME is '整合名称'
/

comment on column DI_CATEGORY.CATEGORY_PID is '父级id'
/

comment on column DI_CATEGORY.IS_DEFAULT is '1不允许删除，0允许删除'
/

comment on column DI_CATEGORY.REP_ID is '资源库id'
/

comment on column DI_CATEGORY.PATH is '文件路径'
/

comment on column DI_CATEGORY.CATEGORY_ID is '对应资源库目录id'
/

comment on column DI_CATEGORY.CODE is '树层级编码'
/

create table DI_SCRIPT
(
    ID          VARCHAR2(32) not null
        constraint SYS_C0092967
        primary key
        constraint SYS_C0092966
        check ("ID" IS NOT NULL),
    NAME        VARCHAR2(255),
    TYPE        VARCHAR2(255),
    PATH        VARCHAR2(255),
    CATEGORY_ID VARCHAR2(32),
    SCRIPT_ID   VARCHAR2(32),
    REP_ID      VARCHAR2(32),
    CREATE_DATE DATE
)
    /

comment on column DI_SCRIPT.ID is '主键id'
/

comment on column DI_SCRIPT.NAME is '名称'
/

comment on column DI_SCRIPT.TYPE is '类型'
/

comment on column DI_SCRIPT.PATH is '脚本路径'
/

comment on column DI_SCRIPT.CATEGORY_ID is '整合分类id'
/

comment on column DI_SCRIPT.SCRIPT_ID is '脚本id'
/

comment on column DI_SCRIPT.REP_ID is '资源库id'
/

comment on column DI_SCRIPT.CREATE_DATE is '创建时间'
/

