-- auto-generated definition
create table K_USER
(
    ID        VARCHAR2(32) not null
        constraint SYS_C0020109
        primary key
        constraint SYS_C0088435
        check ("ID" IS NOT NULL),
    NICKNAME  NVARCHAR2(50),
    EMAIL     NVARCHAR2(30),
    PHONE     NVARCHAR2(50),
    ACCOUNT   NVARCHAR2(50),
    PASSWORD  NVARCHAR2(500),
    ADD_TIME  DATE,
    ADD_USER  NUMBER(11),
    EDIT_TIME DATE,
    EDIT_USER NUMBER(11),
    DEL_FLAG  NUMBER(11)
)
    /

comment on table K_USER is '用户表'
/

comment on column K_USER.ID is '用户ID'
/

comment on column K_USER.NICKNAME is '用户昵称'
/

comment on column K_USER.EMAIL is '用户邮箱'
/

comment on column K_USER.PHONE is '用于电话'
/

comment on column K_USER.ACCOUNT is '用户账号'
/

comment on column K_USER.PASSWORD is '用户密码'
/

comment on column K_USER.ADD_TIME is '添加时间'
/

comment on column K_USER.ADD_USER is '添加者'
/

comment on column K_USER.EDIT_TIME is '编辑时间'
/

comment on column K_USER.EDIT_USER is '编辑者'
/

comment on column K_USER.DEL_FLAG is '是否删除（0：存在；1：删除）'
/

create unique index UNIQUE_INDEX_ACCOUNT
    on K_USER (ACCOUNT)
/

